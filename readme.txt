+------------------------------+-----------------------------------+----+
|file                          |function                           |note|
+------------------------------+-----------------------------------+----+
|barbecue-1.5-beta1.jar        |barcode purpose                    |    |
|c3p0-0.9.1.2.jar              |database purpose                   |lite|
|commons-beanutils-1.9.2.jar   |sometime needed by jasper          |    |
|commons-codec-1.9.jar         |sometime needed by jasper          |    |
|commons-collections-3.2.1.jar |needed by jasper                   |lite|
|commons-digester-2.1.jar      |needed by jasper                   |lite|
|commons-net-3.4.jar           |ftp connector                      |    |
|groovy-all-2.4.5.jar          |sometime needed by jasper          |    |
|itext-2.1.7.jar               |needed by jasper to export to pdf  |    |
|jasperreports-6.1.1.jar       |jasper connector                   |lite|
|javax.mail-1.5.5.jar          |mail connector                     |    |
|junique-1.0.2.jar             |...                                |    |
|jxl-2.6.10.jar                |excel connector                    |lite|
|ojdbc6.jar                    |oracle connector                   |    |
|poi-3.10.1.jar                |needed by jasper to export to xls  |lite|
|sqljdbc4-4.0.jar              |sql server connector               |    |
+------------------------------+-----------------------------------+----+
--eof--